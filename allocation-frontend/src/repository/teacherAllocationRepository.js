import axios from '../axios/axios';

const TeacherAllocationService = {
    fetchJoinedSubjects: () => {
        return axios.get("/joined-subjects");
    },
    fetchSubjectAllocationStats: (semester) => {
        if (semester) {
            return axios.get("/subject-allocation-stats?semester=" + semester)
        }
    },
    fetchTeacherSubjectAllocationsForSemester: (semester) => {
        return axios.get("/teacher-subject-allocations/semester/" + semester);
    },
    fetchTeacherSubjectAllocationsForSubject: (abbreviation) => {
        return axios.get("/teacher-subject-allocations/" + abbreviation)
    },
    updateTeacherSubjectAllocation: (allocation) => {
        return axios.put("/teacher-subject-allocations/edit", allocation)
    },
    updateNumberOfGroupsForSubject: (id, numberOfGroups, mentorshipCourse = false) => {
        return axios.put("/subject-allocation-stats/number-of-groups/" + id + "/" + numberOfGroups + "?mentorshipCourse=" + mentorshipCourse)
    },
    fetchProfessors: () => {
        return axios.get("/professors");
    },
    addTeacherSubjectAllocation: (allocation, semester) => {
        return axios.post("/teacher-subject-allocations/add?semester=" + semester, allocation);
    },
    deleteTeacherSubjectAllocation: (id) => {
        return axios.delete("/teacher-subject-allocations/delete/" + id)
    },
    fetchTeacherPreferences: () => {
        return axios.get("/teacher-subject-requests");
    },
    fetchSemesters: () => {
        return axios.get("/semesters");
    }

}

export default TeacherAllocationService;


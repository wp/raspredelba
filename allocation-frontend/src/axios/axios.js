import axios from "axios";

const instance = axios.create({
    baseURL: "/api",
    headers: {
        'Access-Control-Allow-Origin' : '*',
    },
    auth: {
        username: 'riste.stojanov',
        password: 'SystemPass'
    }
})

export default instance;
import './App.css';
import React from 'react';
import SubjectsComponent from './components/SubjectsComponent';
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
    return (<div>
        <SubjectsComponent/>
        <ToastContainer/>
    </div>);
}

export default App;

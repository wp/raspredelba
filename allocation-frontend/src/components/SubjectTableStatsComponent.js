import React, {useState} from 'react';
import TeacherAllocationService from '../repository/teacherAllocationRepository';
import Tooltip from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import SubjectTableRowComponent from './SubjectTableRowComponent';
import {toast} from 'react-toastify';

function SubjectTableStatsComponent({
                                        subject,
                                        professors,
                                        teacherSubjectAllocations,
                                        setTeacherSubjectAllocations,
                                        hideAllocations,
                                        professorFilter,
                                        calculateAllocatedGroupsToTeachers,
                                        calculateTotalAllocationStatsForTeacher,
                                        getPreferredSubjectNamesForTeacher,
                                        getTeachersWithPreferenceToSubjectAsText,
                                        teacherPreferences,
                                        getTeacherSubjectAllocations,
                                        currentSemester,
                                        updateAllocationStats
                                    }) {
    const [numberOfGroups, setNumberOfGroups] = useState([subject.numberOfGroups])
    const [mentorshipCourse, setMentorshipCourse] = useState(subject.mentorshipCourse || false)
    const [isAdding, setIsAdding] = useState(false);
    const [newAllocation, setNewAllocation] = useState({});
    const [studentsPerGroup, setStudentsPerGroup] = useState(subject.averageStudentsPerGroup)
    const [currentAllocations, setCurrentAllocations] = useState(teacherSubjectAllocations)

    if (!subject.subject) {
        return <></>; // Render nothing if subject.subject is null
    }

    const numberOfFirstTimeStudents = subject.numberOfFirstTimeStudents == null || subject.numberOfFirstTimeStudents == undefined
        ? 0 : subject.numberOfFirstTimeStudents

    const numberOfReEnrollmentStudents = subject.numberOfReEnrollmentStudents == null || subject.numberOfReEnrollmentStudents == undefined
        ? 0 : subject.numberOfReEnrollmentStudents;

    const totalStudents = numberOfFirstTimeStudents + numberOfReEnrollmentStudents;

    const allocationRows = currentAllocations.map((allocation) => (
        <SubjectTableRowComponent
            key={'sub_row_' + allocation.id + "_" + allocation.numberOfLectureGroups + "_" + allocation.numberOfExerciseGroups + "_" + allocation.numberOfLabGroups}
            allocation={allocation}
            calculateTotalAllocationStatsForTeacher={calculateTotalAllocationStatsForTeacher}
            getPreferredSubjectNamesForTeacher={getPreferredSubjectNamesForTeacher}
            getAllocations={getAllocations}
            isSubjectPrefferedByTeacher={isSubjectPrefferedByTeacher}
            handleDelete={handleDelete}
            updateAllocation={updateTeacherAllocation}
        />
    ));
    const renderContent = !hideAllocations ? (
        <tbody>
        {displayAdd()}
        {allocationRows}
        </tbody>
    ) : <></>

    return (
        <div className="subject-table m-3">
            <table className="table table-bordered table-striped">
                <thead>
                <tr>
                    <th className="text-center" colSpan="5">
                        {subject.subject.name}
                        ({subject.subject.weeklyLecturesClasses}+{subject.subject.weeklyAuditoriumClasses}+{subject.subject.weeklyLabClasses})
                        <br/>
                        {subject.numberOfFirstTimeStudents !== undefined ? subject.numberOfFirstTimeStudents : "X"} FTS,
                        {subject.numberOfReEnrollmentStudents !== undefined ? subject.numberOfReEnrollmentStudents : "X"} RES,
                        <input style={{width: '7%'}}
                               type="number"
                               onChange={handleNumberOfGroupsChange}
                               defaultValue={numberOfGroups}/> G
                        (ideally {subject.calculatedNumberOfGroups}),
                        {studentsPerGroup} SPG
                        ( <input type='checkbox' checked={mentorshipCourse} name='mentorshipCourse'
                                 onChange={(e) => setMentorshipCourse(e.target.checked)}/> Menthorship Course)
                    </th>
                    <th className="text-center">
                        <button className='btn btn-success my-2'
                                onClick={handleSave}>Save
                        </button>
                        {displayAddButton()}
                    </th>
                </tr>
                {displayAllocationStats()}
                </thead>
                {renderContent}
            </table>

        </div>
    );

    function displayAddButton() {
        if (hideAllocations || isAdding) {
            return <></>;
        }
        return (
            <button className="btn btn-primary px-3 m-1"
                    onClick={() => setIsAdding(true)}>
                Add
            </button>
        );
    }

    function displayAllocationStats() {
        if (hideAllocations) {
            return <></>;
        }
        const calculated = calculateAllocatedGroupsToTeachers(currentAllocations);
        const colorLecture = calculated.lectureGroups != subject.numberOfGroups ? 'red' : 'black';
        const colorExercise = calculated.exerciseGroups != subject.numberOfGroups ? 'red' : 'black';
        const colorLab = calculated.labGroups != subject.numberOfGroups ? 'red' : 'black';
        return (<tr>
            <th>Professor</th>
            <th style={{
                color: colorLecture
            }}>Lecture groups
                ({calculated.lectureGroups}/{subject.numberOfGroups})
            </th>
            <th style={{
                color: colorExercise
            }}>Exercise groups
                ({calculated.exerciseGroups}/{subject.numberOfGroups})
            </th>
            <th style={{
                color: colorLab
            }}>Lab groups
                ({calculated.labGroups}/{subject.numberOfGroups})
            </th>
            <th>
                English
            </th>
            <th></th>
        </tr>);
    }

    function displayAdd() {
        if (!isAdding) {
            return <></>;
        }
        return (<tr>
            <td>
                <select style={{width: '60%'}} onChange={(e) => handleNewAllocationChange(e, 'professor')}>
                    <option value="">Select a Professor</option>
                    {

                        professors.map((professor) => {
                            const isPreferred = isSubjectPrefferedByTeacher(professor.id);
                            const preferenceInfo = getPreferenceInfo(professor.id);

                            return (
                                <option key={"prof_opt_" + professor.id} value={professor.id}>
                                    {isPreferred ? preferenceInfo : 'no ' + professor.name}
                                </option>
                            )
                        })

                    }
                </select>
                <Tooltip color=''
                         title={<span
                             style={{fontSize: '16px'}}>{getTeachersWithPreferenceToSubjectAsText(subject.subject.abbreviation)}</span>}
                         style={{cursor: 'pointer'}}>
                    <InfoIcon style={{marginLeft: '10px'}}></InfoIcon>
                </Tooltip>
            </td>
            <td>
                <input type="number" defaultValue={0}
                       onChange={(e) => handleNewAllocationChange(e, 'numberOfLectureGroups')}/>
            </td>
            <td>
                <input type="number" defaultValue={0}
                       onChange={(e) => handleNewAllocationChange(e, 'numberOfExerciseGroups')}/>
            </td>
            <td>
                <input type="number" defaultValue={0}
                       onChange={(e) => handleNewAllocationChange(e, 'numberOfLabGroups')}/>
            </td>
            <td>
                <div className='d-flex'>
                    <button className="btn btn-success btn-sm mx-3" onClick={handleAdd}>Save</button>
                    <button className="btn btn-danger btn-sm ml-2" onClick={() => setIsAdding(false)}>Cancel
                    </button>
                </div>
            </td>
        </tr>);
    }

    function getAllocations() {
        getTeacherSubjectAllocations()
    }

    function isSubjectPrefferedByTeacher(professorId) {
        let preferences = teacherPreferences.filter((pref) => pref.professor.id == professorId).map((pref) => pref.subject?.abbreviation)
        return preferences.includes(subject.subject?.abbreviation)
    }

    function getPreferenceInfo(professorId) {
        const teacherPreference = teacherPreferences.find(
            (pref) => pref.professor.id === professorId && pref?.subject?.abbreviation == subject.subject?.abbreviation
        );

        if (!teacherPreference) {
            return '';
        }

        const infoParts = [];
        const order = teacherPreference.priority;

        infoParts.push(`${order}. ${teacherPreference.professor.name}`);

        if (!teacherPreference.preferMultipleGroups) {
            infoParts.push('no mul g');
        }

        if (!teacherPreference.preferAuditoriumExercises) {
            infoParts.push('no exerc');
        }

        if (!teacherPreference.preferLabExercises) {
            infoParts.push('no labs');
        }

        return infoParts.join(', ');
    }

    function handleSave() {
        TeacherAllocationService.updateNumberOfGroupsForSubject(subject.id, numberOfGroups, mentorshipCourse)
            .then(response => {
                setStudentsPerGroup(Math.floor(totalStudents / numberOfGroups))
                subject.numberOfGroups = numberOfGroups
                updateAllocationStats(subject.id, subject)
                toast.success("Successfully updated number of groups!")
            })
            .catch(error => {
                console.log(error);
                toast.error("Error while updating number of groups!")
            })
    };


    function handleNumberOfGroupsChange(e) {
        setNumberOfGroups(e.target.value)
    }


    function handleNewAllocationChange(e, field) {
        const newAllocationCopy = {...newAllocation};
        newAllocationCopy[field] = e.target.value;
        setNewAllocation(newAllocationCopy);
    }

    function handleAdd() {
        if (newAllocation) {
            newAllocation['subject'] = subject.subject.abbreviation
            TeacherAllocationService.addTeacherSubjectAllocation(newAllocation, currentSemester)
                .then((response) => {
                    toast.success("Successfully added allocation!");
                    setCurrentAllocations((prevAllocations) => [...prevAllocations, response.data])
                    setIsAdding(false);
                    setNewAllocation(null)
                    getAllocations()
                })
                .catch((error) => {
                    toast.error("Error while adding allocation!")
                    console.log(error)
                })
        }
    }

    function handleDelete(allocationId) {
        TeacherAllocationService.deleteTeacherSubjectAllocation(allocationId)
            .then((response) => {
                console.log('Allocation deleted successfully:', response.data);
                toast.success("Successfully deleted allocation!")

                // Filter out the deleted allocation and update the state
                setCurrentAllocations((prevAllocations) => prevAllocations.filter((allocation) => allocation.id !== allocationId));

                getAllocations()
            })
            .catch((error) => {
                toast.error("Error while deleting allocation!");
                console.log('Error deleting allocation:', error);
            });
    }

    function updateTeacherAllocation(allocation) {
        setCurrentAllocations((prevAllocations) => prevAllocations.map((a) => {
            if (a.id === allocation.id) {
                return allocation;
            }
            return a;
        }));
    }

}


export default SubjectTableStatsComponent;


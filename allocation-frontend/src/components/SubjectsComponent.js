import React, {Fragment, useEffect, useState} from 'react';
import TeacherAllocationService from '../repository/teacherAllocationRepository';
import SubjectTableStatsComponent from './SubjectTableStatsComponent';
import './SubjectsComponent.css'

function SubjectsComponent() {
    const [subjectAllocationStats, setSubjectAllocationStats] = useState([]);
    const [teacherSubjectAllocations, setTeacherSubjectAllocations] = useState([])
    const [professors, setProfessors] = useState([])
    const [subjects, setSubjects] = useState([])
    const [hideAllocations, setHideAllocations] = useState(false);
    const [professorSearch, setProfessorSearch] = useState('');
    const [subjectSearch, setSubjectSearch] = useState('');
    const [currentSubjectAllocationStats, setCurrentSubjectAllocationStats] = useState([])
    const [teacherPreferences, setTeacherPreferences] = useState([])
    const [semesters, setSemesters] = useState([])
    const [currentSemester, setCurrentSemester] = useState([])


    useEffect(() => {
        TeacherAllocationService.fetchSubjectAllocationStats(currentSemester)
            .then((response) => {
                setSubjectAllocationStats(response.data);
                setCurrentSubjectAllocationStats(response.data)
            })
            .catch((error) => {
                console.log(error);
            });

        TeacherAllocationService.fetchProfessors()
            .then((response) => {
                setProfessors(response.data)
            })
            .catch((error) => {
                console.log(error)
            })

        TeacherAllocationService.fetchJoinedSubjects()
            .then((response) => {
                setSubjects(response.data);
            })
            .catch((error) => {
                console.log(error);
            });

        TeacherAllocationService.fetchTeacherPreferences()
            .then((response) => {
                setTeacherPreferences(response.data)
            })
            .catch((error) => {
                console.log(error)
            })

        TeacherAllocationService.fetchSemesters()
            .then((response) => {
                setCurrentSemester(response.data[response.data.length - 1].code)
                setSemesters(response.data)
            })
            .catch((error) => {
                console.log(error)
            })

    }, []);

    useEffect(() => {
        if (currentSemester) {
            getTeacherSubjectAllocations()
            fetchStatsForSemester(currentSemester)
        }
    }, [currentSemester])


    return (
        <div className="subjects-container">
            <header>
                <nav className="navbar navbar-expand-lg navbar-dark bg-secondary">
                    <div className="container">

                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav ms-auto">
                                <li className="nav-item">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Search by teacher"
                                        value={professorSearch}
                                        onChange={(e) => handleSearchByProfessor(e.target.value)}
                                    />
                                </li>
                                <li className="nav-item">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Search by subject"
                                        value={subjectSearch}
                                        onChange={(e) => handleSearchBySubject(e.target.value)}
                                    />
                                </li>
                                <li className='nav-item'>
                                    <select
                                        onChange={(e) => {
                                            setCurrentSemester(e.target.value);
                                        }}
                                        className="filter-select"
                                        value={currentSemester + ''}
                                        style={filterSelectStyle}
                                    >
                                        {semesters.map((sem) => (
                                            <option key={'sem_' + sem.code} value={sem.code}>
                                                {sem.year} {sem.semesterType}
                                            </option>
                                        ))}
                                    </select>
                                </li>
                                <li className='nav-item'>
                                    <input
                                        type="checkbox"
                                        checked={hideAllocations}
                                        onChange={handleCheckboxChange}
                                        style={{marginRight: '3px'}}
                                    />
                                    <label>
                                        Hide teacher allocations
                                    </label>
                                </li>
                                <li className='nav-item'>
                                    <select onChange={(e) => {
                                        applyFilter(e.target.value);
                                    }} className="filter-select" style={filterSelectStyle}>
                                        <option value="">---------</option>
                                        <option value="golemi-grupi">Предмети со големи групи</option>
                                        <option value="visok-profesori">Предмети со вишок професори</option>
                                        <option value="visok-asistenti">Предмети со вишок асистенти</option>
                                        <option value="nepokrieni-predavanja">Предмети со непокриени предавања</option>
                                        <option value="nepokrieni-vezbi">Предмети со непокриени вежби</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <div className="container mt-4">
                <h2 className="text-center mb-4">Subject Allocation Statistics</h2>
                <div className="row justify-content-center text-center">
                    {currentSubjectAllocationStats.map((subject) => {
                        var filteredAllocations = teacherSubjectAllocations.filter(
                            allocation => allocation.subject?.abbreviation === subject.subject?.abbreviation
                        );

                        if (professorSearch) {
                            filteredAllocations = filteredAllocations.filter(
                                allocation => allocation.professor?.name.toLowerCase().includes(professorSearch.toLowerCase())
                            )
                        }
                        const stats = calculateTotalAllocationStatsForTeacher(teacherSubjectAllocations);
                        const subjectKey = 'subject_' + subject.id + "_" + subject.numberOfGroups
                            + "_" + stats.lectureGroups
                            + "_" + stats.exerciseGroups
                            + "_" + stats.exerciseGroups;
                        return (
                            <SubjectTableStatsComponent
                                key={subjectKey}
                                subject={subject}
                                professors={professors}
                                teacherSubjectAllocations={filteredAllocations}
                                setTeacherSubjectAllocations={setTeacherSubjectAllocations}
                                hideAllocations={hideAllocations || filteredAllocations.length === 0}
                                professorFilter={professorSearch}
                                calculateAllocatedGroupsToTeachers={calculateNumberOfAllocatedGroups}
                                calculateTotalAllocationStatsForTeacher={calculateTotalAllocationStatsForTeacher}
                                getPreferredSubjectNamesForTeacher={getPreferredSubjectNamesForTeacher}
                                getTeachersWithPreferenceToSubjectAsText={getTeachersWithPreferenceToSubjectAsText}
                                teacherPreferences={teacherPreferences}
                                getTeacherSubjectAllocations={getTeacherSubjectAllocations}
                                currentSemester={currentSemester}
                                updateAllocationStats={updateAllocationStats}
                            />
                        );
                    })}

                </div>
            </div>
        </div>
    );

    function updateAllocationStats(id, stats) {
        setSubjectAllocationStats(subjectAllocationStats.map(subject => {
            if (subject.id === id) {
                subject = stats;
            }
            return subject;
        }))
    }


    function fetchStatsForSemester(semester) {
        TeacherAllocationService.fetchSubjectAllocationStats(semester)
            .then((response) => {
                setSubjectAllocationStats(response.data);
                setCurrentSubjectAllocationStats(response.data)
            })
            .catch((error) => {
                console.log(error);
            });
    }

    function getTeacherSubjectAllocations() {
        TeacherAllocationService.fetchTeacherSubjectAllocationsForSemester(currentSemester)
            .then((response) => {
                setTeacherSubjectAllocations(response.data)
            })
            .catch((error) => {
                console.log(error)
            })
    }

    function handleCheckboxChange() {
        setHideAllocations(!hideAllocations);
    }

    function getPreferredSubjectNamesForTeacher(id) {
        return (
            <>
                <b>Преференци:</b>
                {teacherPreferences.filter(pref => pref.professor.id === id).map((pref, index) => <p
                    key={'pref' + pref.professor.id + '_' + pref.subect?.id}>- {pref.subject?.name}</p>)}
            </>
        )
    }

    function getTeachersWithPreferenceToSubjectAsText(subjectId) {
        return (
            <>
                <b>Преферираат:</b>
                {teacherPreferences.filter(pref => pref.subject?.abbreviation === subjectId).map(pref =>
                    <p key={'pos_pref_' + pref.professor.id + '_' + pref.subect?.id}>- {pref.professor.name}</p>)}
            </>
        )
    }

    function calculateTotalAllocationStatsForTeacher(professorId) {
        var exercGroups = 0.0;
        var lectureGroups = 0.0;
        var labGroups = 0.0;
        for (let i = 0; i < teacherSubjectAllocations.length; i++) {
            if (teacherSubjectAllocations[i].professor.id == professorId) {
                exercGroups += parseFloat(teacherSubjectAllocations[i]?.numberOfExerciseGroups) || 0
                lectureGroups += parseFloat(teacherSubjectAllocations[i]?.numberOfLectureGroups) || 0
                labGroups += parseFloat(teacherSubjectAllocations[i]?.numberOfLabGroups) || 0
            }
        }

        return {
            "exerciseGroups": exercGroups.toFixed(1),
            "lectureGroups": lectureGroups.toFixed(1),
            "labGroups": labGroups.toFixed(1)
        }
    }

    function calculateNumberOfAllocatedGroups(allocations) {
        var exercGroups = 0.0;
        var lectureGroups = 0.0;
        var labGroups = 0.0;
        for (let i = 0; i < allocations.length; i++) {
            exercGroups += parseFloat(allocations[i]?.numberOfExerciseGroups) || 0
            lectureGroups += parseFloat(allocations[i]?.numberOfLectureGroups) || 0
            labGroups += parseFloat(allocations[i]?.numberOfLabGroups) || 0
        }

        return {
            "exerciseGroups": exercGroups.toFixed(1),
            "lectureGroups": lectureGroups.toFixed(1),
            "labGroups": labGroups.toFixed(1)
        }
    }


    function applyFilter(value) {
        if (value == "golemi-grupi") {
            setCurrentSubjectAllocationStats(
                subjectAllocationStats.filter((allocation) => allocation.averageStudentsPerGroup > 150)
            )
        } else if (value == "visok-profesori") {
            setCurrentSubjectAllocationStats(
                subjectAllocationStats.filter((allocation) => {

                    const allocatedGroups = calculateNumberOfAllocatedGroups(
                        teacherSubjectAllocations.filter(
                            (teacherAllocation) =>
                                teacherAllocation.subject?.name === allocation.subject?.name
                        )
                    );

                    return allocatedGroups.lectureGroups > allocation.numberOfGroups;
                })
            );
        } else if (value == "visok-asistenti") {
            setCurrentSubjectAllocationStats(
                subjectAllocationStats.filter((allocation) => {

                    const allocatedGroups = calculateNumberOfAllocatedGroups(
                        teacherSubjectAllocations.filter(
                            (teacherAllocation) =>
                                teacherAllocation.subject?.name === allocation.subject?.name
                        )
                    );

                    return allocatedGroups.exerciseGroups > allocation.numberOfGroups;
                })
            );
        } else if (value == "nepokrieni-predavanja") {
            setCurrentSubjectAllocationStats(
                subjectAllocationStats.filter((allocation) => {

                    const allocatedGroups = calculateNumberOfAllocatedGroups(
                        teacherSubjectAllocations.filter(
                            (teacherAllocation) =>
                                teacherAllocation.subject?.name === allocation.subject?.name
                        )
                    );

                    return allocatedGroups.lectureGroups < allocation.numberOfGroups;
                })
            );
        } else if (value == "nepokrieni-vezbi") {
            setCurrentSubjectAllocationStats(
                subjectAllocationStats.filter((allocation) => {

                    const allocatedGroups = calculateNumberOfAllocatedGroups(
                        teacherSubjectAllocations.filter(
                            (teacherAllocation) =>
                                teacherAllocation.subject?.name === allocation.subject?.name
                        )
                    );

                    return allocatedGroups.exerciseGroups < allocation.numberOfGroups;
                })
            );
        } else {
            setCurrentSubjectAllocationStats(subjectAllocationStats)
        }

    }

    function handleSearchBySubject(searchValue) {

        setSubjectSearch(searchValue)
        const filterFunction = (subject) => {
            // Convert subject name and filter to lowercase for case-insensitive search
            const subjectName = subject.subject?.name.toLowerCase();
            const searchTerm = searchValue.toLowerCase();

            // Check if the subject name contains the search query
            if (subjectName) {
                return subjectName.includes(searchTerm);
            }

            return false;
        };


        const filteredStats = subjectAllocationStats.filter(filterFunction);

        setCurrentSubjectAllocationStats(filteredStats);
    }

    function handleSearchByProfessor(searchValue) {

        setProfessorSearch(searchValue)

        const filterFunction = (subject) => {
            // Convert subject name and filter to lowercase for case-insensitive search
            const subjectName = subject.subject?.name.toLowerCase();
            const searchedProfessor = searchValue.toLowerCase();

            for (let i = 0; i < teacherSubjectAllocations.length; i++) {
                if (teacherSubjectAllocations[i].subject?.abbreviation == subject?.subject?.abbreviation) {
                    if (teacherSubjectAllocations[i].professor.name.toLowerCase().includes(searchedProfessor)) {
                        return true;
                    }
                }
            }

            return false;
        };

        const filteredStats = subjectAllocationStats.filter(filterFunction);

        setCurrentSubjectAllocationStats(filteredStats);
    }

}

export default SubjectsComponent;


const filtersContainerStyle = {
    background: '#f3f3f3',
    padding: '10px',
    justifyContent: 'end',
};

const filterInputStyle = {
    padding: '6px',
    border: 'none',
    // borderRadius: '5px',
    marginLeft: '10px',
    marginRight: '10px',
};

const filterSelectStyle = {
    padding: '6px 12px',
    borderRadius: '5px',
    marginRight: '10px',
};

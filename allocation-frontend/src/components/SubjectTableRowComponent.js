import React, {useState} from 'react';
import TeacherAllocationService from '../repository/teacherAllocationRepository';
import Tooltip from '@mui/material/Tooltip';
import {toast} from 'react-toastify';


export default function SubjectTableRowComponent({
                                                     allocation,
                                                     calculateTotalAllocationStatsForTeacher,
                                                     getPreferredSubjectNamesForTeacher,
                                                     getAllocations,
                                                     isSubjectPrefferedByTeacher,
                                                     handleDelete,
                                                     updateAllocation
                                                 }) {
    const [currentAllocation, setCurrentAllocation] = useState(allocation)



    const shouldRemoveSubject = isSubjectPrefferedByTeacher(currentAllocation.professor.id) ? '' : 'text-danger'

    return (
        <tr key={currentAllocation.id}>
            <td>
                <Tooltip color=''
                         title={getPreferredSubjectNamesForTeacher(currentAllocation.professor.id)}
                         style={{cursor: 'pointer'}}>
                    <a style={{
                        textDecoration: 'none',
                        cursor: 'pointer'
                    }}>
                        <b className={shouldRemoveSubject}>{currentAllocation.professor.name} </b>
                    </a>
                </Tooltip>
                <i>({calculateTotalAllocationStatsForTeacher(currentAllocation.professor.id).lectureGroups}Pred/
                    {calculateTotalAllocationStatsForTeacher(allocation.professor.id).exerciseGroups}Aud/
                    {calculateTotalAllocationStatsForTeacher(allocation.professor.id).labGroups}Lab
                    )</i></td>
            <td>
                <input type='number' value={currentAllocation.numberOfLectureGroups} name='numberOfLectureGroups'
                       onChange={(e) => handleInputChange(e)}/>
            </td>
            <td>
                <input type='number' value={currentAllocation.numberOfExerciseGroups} name='numberOfExerciseGroups'
                       onChange={(e) => handleInputChange(e)}/>
            </td>
            <td>
                <input type='number' value={currentAllocation.numberOfLabGroups} name='numberOfLabGroups'
                       onChange={(e) => handleInputChange(e)}/>
            </td>
            <td>
                <input type='checkbox' checked={currentAllocation.englishGroup} name='englishGroup'
                       onChange={(e) => handleCheckboxChange(e)}/>
            </td>
            <td>
                <button className="btn btn-success btn-sm mx-3" onClick={handleSave}>Save</button>
                <button className="btn btn-danger btn-sm ml-2"
                        onClick={() => handleDelete(currentAllocation.id)}>Delete
                </button>
            </td>
        </tr>
    );


    function handleInputChange(e) {
        const {name, value} = e.target;
        setCurrentAllocation((prevAllocation) => ({
            ...prevAllocation,
            [name]: value,
        }));
    }

    function handleCheckboxChange(e) {
        const {name, checked} = e.target;
        setCurrentAllocation((prevAllocation) => ({
            ...prevAllocation,
            [name]: checked,
        }));
    }

    function handleSave() {
        TeacherAllocationService.updateTeacherSubjectAllocation(currentAllocation)
            .then((response) => {
                toast.success("Successfully updated allocation!");
                updateAllocation(currentAllocation);
            })
            .catch((error) => {
                console.log(error);
                toast.error("Error while updating allocation!");
            })
    }
}

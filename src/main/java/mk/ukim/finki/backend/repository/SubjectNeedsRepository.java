package mk.ukim.finki.backend.repository;

import org.springframework.stereotype.Repository;
import mk.ukim.finki.backend.model.SubjectNeedsView;

@Repository
public interface SubjectNeedsRepository extends JpaSpecificationRepository<SubjectNeedsView, String> {
}
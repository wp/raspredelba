package mk.ukim.finki.backend.repository;

import mk.ukim.finki.backend.model.Room;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RoomRepository extends JpaSpecificationRepository<Room, String> {

    List<Room> findByCapacityGreaterThanOrderByCapacityAsc(Long capacity);
}


package mk.ukim.finki.backend.repository;

import mk.ukim.finki.backend.model.schedule.CourseSize;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseSizeRepository extends JpaSpecificationRepository<CourseSize, String> {

    List<CourseSize> findAllBySemesterCodeAndJoinedSubjectAbbreviationAndGroupNameIn(String code, String abbreviation, List<String> collect);

    List<CourseSize> findAllBySemesterCodeAndJoinedSubjectAbbreviation(String code, String abbreviation);

    List<CourseSize> findCourseSizesByJoinedSubjectAbbreviation(String abbreviation);

}
package mk.ukim.finki.backend.repository;

import mk.ukim.finki.backend.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectRepository extends JpaSpecificationRepository<Subject, String> {
}

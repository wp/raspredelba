package mk.ukim.finki.backend.repository;

import mk.ukim.finki.backend.model.Professor;
import mk.ukim.finki.backend.model.ProfessorTitle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfessorRepository extends JpaSpecificationRepository<Professor, String> {
    List<Professor> findByTitle(ProfessorTitle title);
}

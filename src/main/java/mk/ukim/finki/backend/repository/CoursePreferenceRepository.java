package mk.ukim.finki.backend.repository;

import mk.ukim.finki.backend.model.CoursePreference;
import mk.ukim.finki.backend.model.JoinedSubject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CoursePreferenceRepository extends JpaSpecificationRepository<CoursePreference, String> {

    Page<CoursePreference> findBySubject(JoinedSubject subject, Pageable pageable);

    List<CoursePreference> findCoursePreferencesBySubject_Abbreviation(String abbreviation);
}


package mk.ukim.finki.backend.repository;

import mk.ukim.finki.backend.model.engagement.ProfessorEngagement;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessorEngagementRepository extends JpaSpecificationRepository<ProfessorEngagement, String> {

    Long countBySemester_Code(String semesterCode);
}

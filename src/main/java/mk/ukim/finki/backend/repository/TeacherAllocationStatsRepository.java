package mk.ukim.finki.backend.repository;

import mk.ukim.finki.backend.model.CoursePreference;
import mk.ukim.finki.backend.model.JoinedSubject;
import mk.ukim.finki.backend.model.TeacherAllocationStats;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;


@Repository
public interface TeacherAllocationStatsRepository extends JpaSpecificationRepository<TeacherAllocationStats, String> {

}


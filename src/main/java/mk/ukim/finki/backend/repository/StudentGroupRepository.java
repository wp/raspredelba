package mk.ukim.finki.backend.repository;

import mk.ukim.finki.backend.model.Semester;
import mk.ukim.finki.backend.model.schedule.StudentGroup;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentGroupRepository extends JpaSpecificationRepository<StudentGroup, Long> {

    List<StudentGroup> findAllBySemester(Semester semester);

    List<StudentGroup> findAllBySemesterCode(String semester);

    Optional<StudentGroup> findFirstBySemesterCodeAndName(String semester, String name);

    List<StudentGroup> findAllByIdIn(List<Long> ids);
}
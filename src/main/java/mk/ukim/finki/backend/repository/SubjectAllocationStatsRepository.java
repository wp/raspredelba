package mk.ukim.finki.backend.repository;

import mk.ukim.finki.backend.model.JoinedSubject;
import mk.ukim.finki.backend.model.Semester;
import mk.ukim.finki.backend.model.SubjectAllocationStats;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface SubjectAllocationStatsRepository extends JpaSpecificationRepository<SubjectAllocationStats, String> {
    Optional<SubjectAllocationStats> findFirstBySubjectAndSemester(JoinedSubject joinedSubject, Semester prevSemester);

    List<SubjectAllocationStats> findBySemesterCode(String semester);
}

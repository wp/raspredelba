package mk.ukim.finki.backend.repository;

import mk.ukim.finki.backend.model.StudyProgram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudyProgramRepository extends JpaSpecificationRepository<StudyProgram, String> {
}

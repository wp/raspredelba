package mk.ukim.finki.backend.repository;

import mk.ukim.finki.backend.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaSpecificationRepository<Student, String> {
}

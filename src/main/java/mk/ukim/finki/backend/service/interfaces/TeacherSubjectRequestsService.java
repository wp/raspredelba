package mk.ukim.finki.backend.service.interfaces;


import mk.ukim.finki.backend.model.JoinedSubject;
import mk.ukim.finki.backend.model.Professor;
import mk.ukim.finki.backend.model.SemesterType;
import mk.ukim.finki.backend.model.TeacherSubjectRequests;
import mk.ukim.finki.backend.model.DTO.JoinedSubjectDTO;
import mk.ukim.finki.backend.model.DTO.TeacherSubjectRequestsDTO;
import mk.ukim.finki.backend.model.exceptions.InvalidTeacherSubjectRequestIdException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TeacherSubjectRequestsService {
    List<TeacherSubjectRequests> getAllTeacherSubjectRequests();

    TeacherSubjectRequests getTeacherSubjectRequestById(Long id) throws InvalidTeacherSubjectRequestIdException;

    TeacherSubjectRequests saveTeacherSubjectRequest(TeacherSubjectRequests teacherSubjectRequest);

    void deleteTeacherSubjectRequest(Long id);

    Page<TeacherSubjectRequests> getTeacherSubjectRequestsByPage(int page, int size);

    Page<TeacherSubjectRequests> filter(String professor,
                                        String subjectId,
                                        SemesterType semesterType,
                                        int page, int size);

    List<TeacherSubjectRequests> getTeacherSubjectRequestsByProfessorId(String professorId);

    List<TeacherSubjectRequests> getTeacherSubjectRequestsBySubject(JoinedSubject subject);

    List<TeacherSubjectRequestsDTO> importTeacherSubjectRequests(List<TeacherSubjectRequestsDTO> teacherSubjectRequests);
}

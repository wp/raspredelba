package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.SubjectNeedsView;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SubjectNeedsService {
    Page<SubjectNeedsView> list(String semesterCode, String name,
                                int page,
                                int results,
                                Boolean lessThanPRG,
                                Boolean lessThanPR,
                                Boolean lessThanARG,
                                Boolean lessThanAR,
                                Boolean lessThanCL
    );

    List<SubjectNeedsView> findAllForExport(String semesterCode,
                                            String name,
                                            Boolean lessThanPRG,
                                            Boolean lessThanPR,
                                            Boolean lessThanARG,
                                            Boolean lessThanAR,
                                            Boolean lessThanCL
    );
}

package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.StudyProgram;

import java.util.List;

public interface StudyProgramService {

    List<StudyProgram> findAll();
}

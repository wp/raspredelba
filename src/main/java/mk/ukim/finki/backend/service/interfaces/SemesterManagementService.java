package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.Semester;
import mk.ukim.finki.backend.model.SemesterState;
import mk.ukim.finki.backend.model.SemesterType;
import mk.ukim.finki.backend.model.enums.StudyCycle;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface SemesterManagementService {
    Semester saveSemester(String year, SemesterType semesterType,
                          LocalDate startDate,
                          LocalDate endDate, LocalDate enrollmentStartDate,
                          LocalDate enrollmentEndDate,
                          List<StudyCycle> cycles,
                          SemesterState state);
    Optional<Semester> updateSemester(String code, String year, SemesterType semesterType,
                        LocalDate startDate,
                        LocalDate endDate, LocalDate enrollmentStartDate,
                        LocalDate enrollmentEndDate,
                        List<StudyCycle> cycles,
                        SemesterState state);

    List<Semester> getAllSemesters();

    Optional<Semester> getSemesterById(String code);

    Semester getLastSemester();

    Page<Semester> list(int page, int results);

    Semester getActiveSemester();

}

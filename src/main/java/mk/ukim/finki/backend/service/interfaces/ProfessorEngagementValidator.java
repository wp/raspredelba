package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.engagement.ProfessorEngagement;

public interface ProfessorEngagementValidator {
    String validateAndUpdateValidationMessage(ProfessorEngagement engagement, Float number);
}

package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.ProfessorTitle;
import mk.ukim.finki.backend.model.TeacherAllocationStats;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TeacherAllocationStatsService {

    Page<TeacherAllocationStats> list(String semesterCode,
                                      String subjectAbbreviation,
                                      String professorId,
                                      ProfessorTitle professorTitle,
                                      int page,
                                      int results);

    List<TeacherAllocationStats> findAll();
}

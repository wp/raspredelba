package mk.ukim.finki.backend.service.validators;


import mk.ukim.finki.backend.model.engagement.ProfessorEngagement;
import mk.ukim.finki.backend.service.interfaces.ProfessorEngagementValidator;
import org.springframework.stereotype.Component;

@Component
public class NumberOfClassesValidator implements ProfessorEngagementValidator {

    @Override
    public String validateAndUpdateValidationMessage(ProfessorEngagement engagement, Float newNumberOfClasses) {
        StringBuilder validationMessage = new StringBuilder();

        Float currentNumberOfClasses = engagement.getNumberOfClasses();
        if (!currentNumberOfClasses.equals(newNumberOfClasses)) {
            validationMessage.append("The number of classes has been changed.\n");
        }
        return validationMessage.toString();
    }
}
package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.schedule.CourseSize;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CourseSizeService {

    Page<CourseSize> list(String semester, String joinedSubject, Long groupId, Integer pageNum, Integer results);

    List<CourseSize> getCourseSizesBySubject(String abbreviation);


    List<CourseSize> getCourseSizesBySubjectAndSemester(String abbreviation, String semesterCode);
}

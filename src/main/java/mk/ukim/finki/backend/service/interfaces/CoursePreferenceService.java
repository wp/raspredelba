package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.*;
import mk.ukim.finki.backend.model.DTO.CoursePreferenceDTO;
import mk.ukim.finki.backend.model.exceptions.InvalidCoursePreferenceIdException;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;


import java.util.List;

public interface CoursePreferenceService {
    List<CoursePreferenceDTO> importCoursePreferences(List<CoursePreferenceDTO> importPreferences, String semester);

    CoursePreference getCoursePreferenceById(String id) throws InvalidCoursePreferenceIdException;

    CoursePreference saveCoursePreference(JoinedSubject subject,
                                          LectureSharing lectureSharing,
                                          LectureSharing auditoriumExercisesSharing,
                                          boolean preferOnlineLectures,
                                          boolean preferOnlineExercises,
                                          boolean labExercisesAsConsultations);

    CoursePreference save(CoursePreference preference);

    void deleteCoursePreference(String id);


    List<CoursePreference> getAllCoursePreferences();


    Page<CoursePreference> filterAndPaginateCoursePreferences(String subjectId, int pageNum, int results);

    List<CoursePreference> getCoursePreferencesBySubject(String abbreviation);

    Page<CoursePreference> list(Specification<CoursePreference> spec, int page, int size);
}


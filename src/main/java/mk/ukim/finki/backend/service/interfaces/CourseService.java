package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.DTO.CourseDto;
import mk.ukim.finki.backend.model.schedule.Course;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CourseService {

    void calculateGroups(String semesterCode);

    Page<Course> list(String semesterCode, String subject, String groupName, String professor, Integer pageNum, Integer results);

    List<CourseDto> importData(List<CourseDto> students);

    void save(String id, String professors, String assistants, String groups, Boolean english);

    String toTsv(List<Course> groups);

    void delete(String id);

    void save(String semesterCode, String subject, String professors, String assistants, String groups);

    void createAndCalculate(String semesterCode, String subject);

    List<Course> getCoursesByProfessorId(String professorId);

    List<Course> getCoursesBySubject(String abbreviation);

    List<Course> getCoursesBySubjectAndSemester(String abbreviation, String semesterCode);

    List<Course> getCoursesByProfessorIdAndSemester(String professorId, String assistantId, String semesterCode);
}

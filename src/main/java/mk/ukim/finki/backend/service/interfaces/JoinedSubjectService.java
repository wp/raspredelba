package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.DTO.JoinedSubjectDTO;
import mk.ukim.finki.backend.model.DTO.JoinedSubjectProfessorsDTO;
import mk.ukim.finki.backend.model.DTO.JoinedSubjectTeacherRequestDTO;
import mk.ukim.finki.backend.model.JoinedSubject;
import mk.ukim.finki.backend.model.SemesterType;
import mk.ukim.finki.backend.model.exceptions.InvalidJoinedSubjectAbbreviationException;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.util.List;

public interface JoinedSubjectService {


    JoinedSubject getByAbbreviation(String abbreviation) throws InvalidJoinedSubjectAbbreviationException;

    JoinedSubject save(JoinedSubject joinedSubject);

    boolean existsByAbbreviation(String abbreviation);

    void updateAbbreviation(String abbreviation, String newAbbreviation);

    void delete(String abbreviation);

    Page<JoinedSubject> filter(String name,
                               String mainSubject,
                               SemesterType semesterType,
                               LocalDate modifiedAfter,
                               int pageNum, int pageSize);

    Page<JoinedSubject> list(int page, int size);

    List<JoinedSubject> getAllJoinedSubjects();

    Page<JoinedSubject> listActivatedSubjects(String name, SemesterType semesterType, int pageNum, int pageSize);

    Page<JoinedSubjectProfessorsDTO> listActivatedSubjectsWithProfessors(String name, SemesterType semesterType, int pageNum, int pageSize);

    List<JoinedSubjectDTO> importJoinedSubjects(List<JoinedSubjectDTO> importSubjects, String semester);

}

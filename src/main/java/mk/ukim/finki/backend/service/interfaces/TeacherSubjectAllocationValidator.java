package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.TeacherSubjectAllocations;

public interface TeacherSubjectAllocationValidator {
    String validateAndUpdateValidationMessage(TeacherSubjectAllocations allocation);
}

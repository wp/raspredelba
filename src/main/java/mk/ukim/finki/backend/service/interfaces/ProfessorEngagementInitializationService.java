package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.ClassType;
import mk.ukim.finki.backend.model.Language;
import mk.ukim.finki.backend.model.Professor;
import mk.ukim.finki.backend.model.engagement.ProfessorEngagement;
import org.springframework.data.domain.Page;

public interface ProfessorEngagementInitializationService {
    void initSemester(String semester);
}

package mk.ukim.finki.backend.service.implementation;


import lombok.AllArgsConstructor;
import mk.ukim.finki.backend.model.Professor;
import mk.ukim.finki.backend.model.ProfessorTitle;
import mk.ukim.finki.backend.model.exceptions.ProfessorNotFoundException;
import mk.ukim.finki.backend.repository.ProfessorRepository;
import mk.ukim.finki.backend.repository.TeacherSubjectAllocationsRepository;
import mk.ukim.finki.backend.service.interfaces.ProfessorService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProfessorServiceImpl implements ProfessorService {
    private final ProfessorRepository professorRepository;
    private final TeacherSubjectAllocationsRepository allocationRepository;

    @Override
    public List<Professor> getAllProfessors() {
        return professorRepository.findAll(Sort.by("email"));
    }

    @Override
    public Professor getProfessorById(String id) throws ProfessorNotFoundException {
        return professorRepository.findById(id)
                .orElseThrow(() -> new ProfessorNotFoundException("Professor with id " + id + " doesn't exist"));
    }
    @Override
    public List<Professor> findProfessorsByTitle(ProfessorTitle title) {
        return professorRepository.findByTitle(title);
    }

    @Override
    public List<Professor> findProfessorsWithAllocations() {
        return allocationRepository.findProfessorsWithAllocations();
    }

}

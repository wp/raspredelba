package mk.ukim.finki.backend.service.implementation;

import mk.ukim.finki.backend.model.StudyProgram;
import mk.ukim.finki.backend.repository.StudyProgramRepository;
import mk.ukim.finki.backend.service.interfaces.StudyProgramService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudyProgramServiceImpl implements StudyProgramService {

    private final StudyProgramRepository studyProgramRepository;

    public StudyProgramServiceImpl(StudyProgramRepository studyProgramRepository) {
        this.studyProgramRepository = studyProgramRepository;
    }

    @Override
    public List<StudyProgram> findAll() {
        return this.studyProgramRepository.findAll();
    }
}

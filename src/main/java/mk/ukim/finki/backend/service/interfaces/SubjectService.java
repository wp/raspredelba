package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.Subject;
import mk.ukim.finki.backend.model.exceptions.SubjectNotFoundException;

import java.util.List;

public interface SubjectService {
    List<Subject> getAllSubjects();

    Subject getSubjectById(String mainSubjectId) throws SubjectNotFoundException;

}
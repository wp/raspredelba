package mk.ukim.finki.backend.service.interfaces;

import mk.ukim.finki.backend.model.Professor;
import mk.ukim.finki.backend.model.ProfessorTitle;
import mk.ukim.finki.backend.model.exceptions.ProfessorNotFoundException;

import java.util.List;

public interface ProfessorService {


    List<Professor> getAllProfessors();

    Professor getProfessorById(String id) throws ProfessorNotFoundException;

    List<Professor> findProfessorsByTitle(ProfessorTitle title);
    List<Professor> findProfessorsWithAllocations();
}

package mk.ukim.finki.backend.web;

import lombok.AllArgsConstructor;
import mk.ukim.finki.backend.model.schedule.CourseSize;
import mk.ukim.finki.backend.repository.ImportRepository;
import mk.ukim.finki.backend.repository.JoinedSubjectRepository;
import mk.ukim.finki.backend.repository.ProfessorRepository;
import mk.ukim.finki.backend.repository.SemesterManagementRepository;
import mk.ukim.finki.backend.service.interfaces.CourseSizeService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@AllArgsConstructor
@Controller
@RequestMapping("/admin/course-size")
public class CourseSizeController {

    private final CourseSizeService service;

    private final ImportRepository importRepository;
    private final JoinedSubjectRepository joinedSubjectRepository;
    private final ProfessorRepository professorRepository;
    private final SemesterManagementRepository semesterRepository;


    @GetMapping
    public String list(Model model,
                       @RequestParam(required = false) String semester,
                       @RequestParam(required = false) String joinedSubject,
                       @RequestParam(required = false) Long groupId,
                       @RequestParam(required = false) String professor,
                       @RequestParam(defaultValue = "1") Integer pageNum,
                       @RequestParam(defaultValue = "20") Integer results) {

        Page<CourseSize> result = service.list(semester, joinedSubject, groupId, pageNum, results);

        model.addAttribute("page", result);
        model.addAttribute("subjects", joinedSubjectRepository.findAll());
        model.addAttribute("professors", professorRepository.findAll());
        model.addAttribute("semesters", semesterRepository.findAll());

        return "course-size/list";
    }

}

package mk.ukim.finki.backend.web;

import mk.ukim.finki.backend.model.SemesterState;
import mk.ukim.finki.backend.model.SemesterType;
import mk.ukim.finki.backend.model.enums.StudyCycle;
import mk.ukim.finki.backend.model.exceptions.InvalidSemesterYearException;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.ui.Model;
import mk.ukim.finki.backend.model.Semester;
import mk.ukim.finki.backend.service.interfaces.SemesterManagementService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/admin/semester")
public class SemesterManagementController {

    private final SemesterManagementService semesterManagementService;

    public SemesterManagementController(SemesterManagementService semesterManagementService) {
        this.semesterManagementService = semesterManagementService;
    }

    @GetMapping
    public String getAllSemesters(Model model,
                                  @RequestParam(defaultValue = "1") Integer pageNum,
                                  @RequestParam(defaultValue = "20") Integer results) {
        List<Semester> semesters = semesterManagementService.getAllSemesters();
        Page<Semester> page = semesterManagementService.list(pageNum,results);
        model.addAttribute("semesters", semesters);
        model.addAttribute("page", page);
        return "semester/listSemesters";
    }

    @GetMapping("/edit/{code}")
    public String showEditSemesterForm(@PathVariable String code, Model model) {
        Semester semester = semesterManagementService.getSemesterById(code).orElseThrow(() -> new IllegalArgumentException(code));
        model.addAttribute("semester", semester);
        return "semester/addSemester";
    }

    @GetMapping("/add")
    public String showSemesterForm() {
        return "semester/addSemester";
    }

    @PostMapping("/save")
    public String saveSemester(@RequestParam String year, @RequestParam SemesterType semesterType, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                               @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
                               @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate enrollmentStartDate,
                               @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate enrollmentEndDate,
                               @RequestParam List<StudyCycle> cycles,
                               @RequestParam SemesterState state) {
        if (year.contains("/"))
            throw new InvalidSemesterYearException(year);
        this.semesterManagementService.saveSemester(year, semesterType, startDate, endDate, enrollmentStartDate, enrollmentEndDate, cycles, state);
        return "redirect:/admin/semester";
    }

    @PostMapping("/save/{code}")
    public String saveSemesterByCode(@PathVariable String code, @RequestParam String year, @RequestParam SemesterType semesterType,
                                     @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                     @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
                                     @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate enrollmentStartDate,
                                     @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate enrollmentEndDate,
                                     @RequestParam List<StudyCycle> cycles,
                                     @RequestParam SemesterState state) {
        this.semesterManagementService.updateSemester(code, year, semesterType, startDate, endDate, enrollmentStartDate, enrollmentEndDate, cycles, state);
        return "redirect:/admin/semester";
    }


}

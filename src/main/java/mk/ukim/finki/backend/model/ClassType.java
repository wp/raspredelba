package mk.ukim.finki.backend.model;

public enum ClassType {
    Lecture,
    Auditory_Exercises,
    Laboratory_Exercises
}

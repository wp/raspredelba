package mk.ukim.finki.backend.model.DTO;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.*;
import lombok.*;
import mk.ukim.finki.backend.model.SemesterType;
import mk.ukim.finki.backend.model.Subject;
import mk.ukim.finki.backend.model.enums.StudyCycle;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"abbreviation", "name", "codes", "semesterType", "mainSubject", "weeklyLecturesClasses", "weeklyAuditoriumClasses", "weeklyLabClasses", "cycle", "validationMessage"})
public class JoinedSubjectDTO {
    private String abbreviation;
    private String name;
    private String codes;
    private String semesterType;
    private String mainSubjectCode;
    private Integer weeklyLecturesClasses;
    private Integer weeklyAuditoriumClasses;
    private Integer weeklyLabClasses;
    private String cycle;
    private String validationMessage;
}

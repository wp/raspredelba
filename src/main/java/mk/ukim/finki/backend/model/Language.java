package mk.ukim.finki.backend.model;

public enum Language {
    Macedonian,
    English
}

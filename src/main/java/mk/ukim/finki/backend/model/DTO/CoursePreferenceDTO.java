package mk.ukim.finki.backend.model.DTO;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.*;
import mk.ukim.finki.backend.model.JoinedSubject;
import mk.ukim.finki.backend.model.LectureSharing;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"name", "lectureSharing", "auditoriumExercisesSharing","preferOnlineLectures","preferOnlineExercises","labExercisesAsConsultations","message"})
public class CoursePreferenceDTO {
    private String name;
    private String lectureSharing;
    private String auditoriumExercisesSharing;
    private boolean preferOnlineLectures;
    private boolean preferOnlineExercises;
    private boolean labExercisesAsConsultations;
    private String message;
}

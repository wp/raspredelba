package mk.ukim.finki.backend.model;

public enum SemesterType {
    WINTER,
    SUMMER,
}

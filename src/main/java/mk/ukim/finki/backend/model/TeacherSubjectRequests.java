package mk.ukim.finki.backend.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedEntityGraph(
        name = "TeacherSubjectRequests.all",
        includeAllAttributes = true
)
public class TeacherSubjectRequests {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Professor professor;

    @JoinColumn(name = "subject_id")
    @ManyToOne
    private JoinedSubject subject;

    private Double priority;

    private Boolean preferMultipleGroups;

    private Boolean preferAuditoriumExercises;

    private Boolean preferLabExercises;

    @Column(length = 2000)
    private String schedulePreferences;

    @Column(length = 2000)
    private String note;

    @ManyToOne
    private Semester startedTeachingFrom;

    @ManyToOne
    private Semester startedExerciseFrom;

    private Boolean acceptsEnglishGroup;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TeacherSubjectRequests that = (TeacherSubjectRequests) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public TeacherSubjectRequests(Professor professor, JoinedSubject subject, Double priority,
            Boolean preferMultipleGroups, Boolean preferAuditoriumExercises, Boolean preferLabExercises,
            String schedulePreferences, String note, Semester startedTeachingFrom, Semester startedExerciseFrom,
            Boolean acceptsEnglishGroup) {
        this.professor = professor;
        this.subject = subject;
        this.priority = priority;
        this.preferMultipleGroups = preferMultipleGroups;
        this.preferAuditoriumExercises = preferAuditoriumExercises;
        this.preferLabExercises = preferLabExercises;
        this.schedulePreferences = schedulePreferences;
        this.note = note;
        this.startedTeachingFrom = startedTeachingFrom;
        this.startedExerciseFrom = startedExerciseFrom;
        this.acceptsEnglishGroup = acceptsEnglishGroup;
    }
}

package mk.ukim.finki.backend.model;

public enum RoomType {
    CLASSROOM,
    LAB,
    MEETING_ROOM,
    OFFICE
}

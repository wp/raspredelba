package mk.ukim.finki.backend.model.DTO;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;
import mk.ukim.finki.backend.model.JoinedSubject;
import mk.ukim.finki.backend.model.Professor;

import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"subject", "professors"})
public class JoinedSubjectProfessorsDTO {
    private JoinedSubject subject;
    private List<Professor> professors;

    public String professorsToString() {
        return professors.stream()
                .map(Professor::getName)
                .collect(Collectors.joining(", "));
    }
}

package mk.ukim.finki.backend.model.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
public class TeacherSubjectAllocationsDTO {

    private String professor;
    private String subject;
    private Float numberOfLectureGroups;
    private Float numberOfExerciseGroups;
    private Float numberOfLabGroups;

}

package mk.ukim.finki.backend.model.DTO;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;
import mk.ukim.finki.backend.model.JoinedSubject;
import mk.ukim.finki.backend.model.TeacherSubjectRequests;

import java.util.List;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"subject", "teacherSubjectRequests"})
public class JoinedSubjectTeacherRequestDTO {
    private JoinedSubject subject;
    private List<TeacherSubjectRequests> teacherSubjectRequests;

}

package mk.ukim.finki.backend.model.enums;

public enum StudyCycle {

    UNDERGRADUATE, MASTER, PHD
}


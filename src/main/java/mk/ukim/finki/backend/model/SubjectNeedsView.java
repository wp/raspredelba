package mk.ukim.finki.backend.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Immutable;

import java.util.Objects;

@Entity
@Table(name = "subject_needs_view")
@Immutable
@Getter
@Setter
@NoArgsConstructor
@JsonPropertyOrder({ "id", "semester_code", "subject_name", "n_groups",
        "n_assigned_professors", "n_req_professors", "n_assigned_assistants",
        "n_req_assistants", "n_calc_groups", "n_covered_groups" })
public class SubjectNeedsView {

    @Id
    private String id;

    private String semesterCode;

    private String subjectName;

    private Integer nGroups;

    private Integer nCalcGroups;

    private Integer nAssignedProfessors;

    private Integer nReqProfessors;

    private Integer nAssignedAssistants;

    private Integer nReqAssistants;

    private Integer nCalcLabGroups;

    private Integer nCoveredLabGroups;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        SubjectNeedsView that = (SubjectNeedsView) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

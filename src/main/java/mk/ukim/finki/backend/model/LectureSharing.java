package mk.ukim.finki.backend.model;

public enum LectureSharing {
    NONE,
    STUDENT_GROUPS,
    TOPICS
}

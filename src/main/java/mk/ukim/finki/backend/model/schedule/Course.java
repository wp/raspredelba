package mk.ukim.finki.backend.model.schedule;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import mk.ukim.finki.backend.model.JoinedSubject;
import mk.ukim.finki.backend.model.Room;
import mk.ukim.finki.backend.model.Semester;

import java.util.List;
import java.util.Optional;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class Course {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Semester semester;

    @ManyToOne
    private JoinedSubject joinedSubject;

    private String professors;

    private String assistants;

    @ManyToMany
    private List<Room> rooms;

    private Integer numberOfFirstEnrollments;

    private Integer numberOfReEnrollments;

    private Float groupPortion = 1.0F;

    private String groups;

    private Boolean english = false;

    public Integer getTotalStudents() {
        return Optional.ofNullable(numberOfFirstEnrollments).orElse(0) +
                Optional.ofNullable(numberOfReEnrollments).orElse(0);
    }

}

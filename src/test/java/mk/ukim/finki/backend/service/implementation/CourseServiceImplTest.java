package mk.ukim.finki.backend.service.implementation;

import mk.ukim.finki.backend.model.*;
import mk.ukim.finki.backend.model.schedule.Course;
import mk.ukim.finki.backend.model.schedule.CourseSize;
import mk.ukim.finki.backend.repository.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class CourseServiceImplTest {

    @Mock private CourseRepository repository;
    @Mock private CourseSizeRepository sizeRepository;
    @Mock private SubjectAllocationStatsRepository subjectAllocationStatsRepository;
    @Mock private StudentGroupRepository studentGroupRepository;
    @Mock private TeacherSubjectAllocationsRepository teacherSubjectAllocationsRepository;
    @Mock private RoomRepository roomRepository;
    @Mock private ProfessorRepository professorRepository;
    @Mock private SemesterManagementRepository semesterManagementRepository;
    @Mock private JoinedSubjectRepository joinedSubjectRepository;
    @Mock private CoursePreferenceRepository coursePreferenceRepository;

    @InjectMocks private CourseServiceImpl courseService;


    /** Builds temporary course for testing.
     * @param sa SubjectAllocationStats object. Initially new object.
     * @param courseSizeList List of groups in course. Initially empty.
     * @param teacherSubjectAllocationsList List of teachers in course. Initially empty.
     * @param numberOfGroups Number of groups.
     * @param lectureSizes List of lecture groups corresponding to teachers.
     * @param exerciseSizes List of exercise groups corresponding to teachers.
     */
    private void buildSubject(SubjectAllocationStats sa,
                              List<CourseSize> courseSizeList,
                              List<TeacherSubjectAllocations> teacherSubjectAllocationsList,
                              int numberOfGroups,
                              List<Float> lectureSizes,
                              List<Float> exerciseSizes) {
        if (numberOfGroups <= 0 || lectureSizes.size() != exerciseSizes.size()) {
            throw new RuntimeException("Invalid call to CourseServiceImplTest.buildSubject");
        }

        JoinedSubject joinedSubject = new JoinedSubject();
        joinedSubject.setAbbreviation("TestCourse");

        sa.setNumberOfGroups(numberOfGroups);
        sa.setSemester(new Semester());
        sa.setSubject(joinedSubject);

        for (int i = 0; i < numberOfGroups; ++i) {
            CourseSize group = new CourseSize();
            group.setJoinedSubjectAbbreviation(joinedSubject.getAbbreviation());
            group.setFirstTimeEnrollments(15L + i);
            group.setReEnrollments(0L);
            courseSizeList.add(group);
        }

        int n = lectureSizes.size();
        int p = 0, a = 0, pa = 0;
        for (int i = 0; i < n; ++i) {
            Professor professor = new Professor();
            if (lectureSizes.get(i) != 0 && exerciseSizes.get(i) == 0) {
                professor.setId("p" + (++p));
            } else if (lectureSizes.get(i) == 0 && exerciseSizes.get(i) != 0) {
                professor.setId("a" + (++a));
            } else if (lectureSizes.get(i) != 0 && exerciseSizes.get(i) != 0) {
                professor.setId("pa" + (++pa));
            } else {
                throw new RuntimeException("Invalid call to CourseServiceImplTest.buildSubject");
            }

            TeacherSubjectAllocations t = new TeacherSubjectAllocations();
            t.setNumberOfLectureGroups(lectureSizes.get(i));
            t.setNumberOfExerciseGroups(exerciseSizes.get(i));
            t.setSubject(joinedSubject);
            t.setProfessor(professor);
            teacherSubjectAllocationsList.add(t);
        }
    }

    @Test
    void testOneGroupWithOneProfessorAndOneAssistant() {
        SubjectAllocationStats sa = new SubjectAllocationStats();
        List<CourseSize> courseSizeList = new ArrayList<>();
        List<TeacherSubjectAllocations> teacherSubjectAllocationsList = new ArrayList<>();

        buildSubject(sa, courseSizeList, teacherSubjectAllocationsList, 1,
                List.of(1F, 0F),
                List.of(0F, 1F)
        );

        CoursePreference coursePreference = new CoursePreference();
        coursePreference.setLectureSharing(LectureSharing.STUDENT_GROUPS);
        coursePreference.setAuditoriumExercisesSharing(LectureSharing.STUDENT_GROUPS);

        when(coursePreferenceRepository.findCoursePreferencesBySubject_Abbreviation(Mockito.any()))
                .thenReturn(List.of(coursePreference));
        when(roomRepository.findByCapacityGreaterThanOrderByCapacityAsc(Mockito.any()))
                .thenReturn(new ArrayList<>());

        List<Course> groups = courseService.calculateGroupsForSubject(sa, courseSizeList, teacherSubjectAllocationsList);

        Assertions.assertEquals(1, groups.size());
        String expectedProfessorID = teacherSubjectAllocationsList.stream()
                .filter(t -> t.getNumberOfLectureGroups() > 0)
                .findFirst().orElse(new TeacherSubjectAllocations())
                .getProfessor()
                .getId();
        String expectedAssistantID = teacherSubjectAllocationsList.stream()
                .filter(t -> t.getNumberOfExerciseGroups() > 0)
                .findFirst().orElse(new TeacherSubjectAllocations())
                .getProfessor()
                .getId();
        Assertions.assertEquals(expectedProfessorID, groups.get(0).getProfessors());
        Assertions.assertEquals(expectedAssistantID, groups.get(0).getAssistants());
    }

    @Test
    void testOneGroupWithOneProfessorAndOneProfessorAssistantWhereOneGroupIsSharedByTwoProfessors() {
        SubjectAllocationStats sa = new SubjectAllocationStats();
        List<CourseSize> courseSizeList = new ArrayList<>();
        List<TeacherSubjectAllocations> teacherSubjectAllocationsList = new ArrayList<>();

        buildSubject(sa, courseSizeList, teacherSubjectAllocationsList, 1,
                List.of(.5F, .5F),
                List.of( 0F,  1F)
        );

        CoursePreference coursePreference = new CoursePreference();
        coursePreference.setLectureSharing(LectureSharing.STUDENT_GROUPS);
        coursePreference.setAuditoriumExercisesSharing(LectureSharing.STUDENT_GROUPS);

        when(coursePreferenceRepository.findCoursePreferencesBySubject_Abbreviation(Mockito.any()))
                .thenReturn(List.of(coursePreference));
        when(roomRepository.findByCapacityGreaterThanOrderByCapacityAsc(Mockito.any()))
                .thenReturn(new ArrayList<>());

        List<Course> groups = courseService.calculateGroupsForSubject(sa, courseSizeList, teacherSubjectAllocationsList);

        Assertions.assertEquals(1, groups.size());
        List<String> professorIDs = List.of(groups.get(0).getProfessors().split(";"));
        teacherSubjectAllocationsList.forEach(teacher ->
                Assertions.assertTrue(professorIDs.contains(teacher.getProfessor().getId()))
        );
        String expectedAssistantID = teacherSubjectAllocationsList.stream()
                .filter(t -> t.getNumberOfExerciseGroups() > 0)
                .findFirst().orElse(new TeacherSubjectAllocations())
                .getProfessor()
                .getId();
        Assertions.assertEquals(expectedAssistantID, groups.get(0).getAssistants());
    }

    @Test
    void testTwoGroupsHavingSharedLecturesWithThreeProfessorsAndTwoAssistants() {
        SubjectAllocationStats sa = new SubjectAllocationStats();
        List<CourseSize> courseSizeList = new ArrayList<>();
        List<TeacherSubjectAllocations> teacherSubjectAllocationsList = new ArrayList<>();

        buildSubject(sa, courseSizeList, teacherSubjectAllocationsList, 2,
                List.of(.67F, .67F, .66F, 0F, 0F),
                List.of(  0F,   0F,   0F, 1F, 1F)
        );

        CoursePreference coursePreference = new CoursePreference();
        coursePreference.setLectureSharing(LectureSharing.TOPICS);
        coursePreference.setAuditoriumExercisesSharing(LectureSharing.STUDENT_GROUPS);

        when(coursePreferenceRepository.findCoursePreferencesBySubject_Abbreviation(Mockito.any()))
                .thenReturn(List.of(coursePreference));
        when(roomRepository.findByCapacityGreaterThanOrderByCapacityAsc(Mockito.any()))
                .thenReturn(new ArrayList<>());

        List<Course> groups = courseService.calculateGroupsForSubject(sa, courseSizeList, teacherSubjectAllocationsList);

        Assertions.assertEquals(2, groups.size());
        for (int i = 0; i < 2; ++i) {
            List<String> professorIDs = List.of(groups.get(i).getProfessors().split(";"));
            teacherSubjectAllocationsList.stream()
                    .filter(teacher -> teacher.getNumberOfLectureGroups() > 0)
                    .forEach(teacher ->
                            Assertions.assertTrue(professorIDs.contains(teacher.getProfessor().getId()))
                    );
        }
        List<String> assistantIDs = new ArrayList<>(List.of(groups.get(0).getAssistants().split(";")));
        assistantIDs.addAll(List.of(groups.get(1).getAssistants().split(";")));
        List<TeacherSubjectAllocations> assistants = teacherSubjectAllocationsList.stream()
                .filter(a -> a.getNumberOfExerciseGroups() > 0)
                .toList();
        String assistant1 = assistants.get(0).getProfessor().getId();
        String assistant2 = assistants.get(1).getProfessor().getId();
        Assertions.assertTrue(assistantIDs.contains(assistant1));
        Assertions.assertTrue(assistantIDs.contains(assistant2));
        Assertions.assertNotEquals(assistant1, assistant2);
    }

    @Test
    void testSevenGroupsWithSevenProfessorsAndFourAssistantsWhereOneGroupIsSharedByTwoProfessors() {
        SubjectAllocationStats sa = new SubjectAllocationStats();
        List<CourseSize> courseSizeList = new ArrayList<>();
        List<TeacherSubjectAllocations> teacherSubjectAllocationsList = new ArrayList<>();

        buildSubject(sa, courseSizeList, teacherSubjectAllocationsList, 7,
                List.of(1F, 1F, 1F, 1F, 1F, 1.5F, .5F, 0F, 0F, 0F, 0F),
                List.of(0F, 0F, 0F, 0F, 0F,   0F,  0F, 3F, 2F, 1F, 1F)
        );

        CoursePreference coursePreference = new CoursePreference();
        coursePreference.setLectureSharing(LectureSharing.STUDENT_GROUPS);
        coursePreference.setAuditoriumExercisesSharing(LectureSharing.STUDENT_GROUPS);

        when(coursePreferenceRepository.findCoursePreferencesBySubject_Abbreviation(Mockito.any()))
                .thenReturn(List.of(coursePreference));
        when(roomRepository.findByCapacityGreaterThanOrderByCapacityAsc(Mockito.any()))
                .thenReturn(new ArrayList<>());

        List<Course> groups = courseService.calculateGroupsForSubject(sa, courseSizeList, teacherSubjectAllocationsList);

        Assertions.assertEquals(7, groups.size());
        List<List<String>> professorIDs = new ArrayList<>();
        List<List<String>> assistantIDs = new ArrayList<>();
        for (int i = 0; i < 7; ++i) {
            professorIDs.add(List.of(groups.get(i).getProfessors().split(";")));
            assistantIDs.add(List.of(groups.get(i).getAssistants().split(";")));
        }
        teacherSubjectAllocationsList.stream()
                .filter(teacher -> teacher.getNumberOfLectureGroups() > 0)
                .forEach(teacher -> Assertions.assertTrue(
                        professorIDs.stream()
                                .flatMap(Collection::stream)
                                .anyMatch(id -> Objects.equals(id, teacher.getProfessor().getId())))
                );
        teacherSubjectAllocationsList.stream()
                .filter(teacher -> teacher.getNumberOfExerciseGroups() > 0)
                .forEach(teacher -> Assertions.assertTrue(
                        assistantIDs.stream()
                                .flatMap(Collection::stream)
                                .anyMatch(id -> Objects.equals(id, teacher.getProfessor().getId())))
                );
        Assertions.assertEquals(7L, professorIDs.stream().flatMap(Collection::stream).distinct().count());
        Assertions.assertEquals(4L, assistantIDs.stream().flatMap(Collection::stream).distinct().count());
        Assertions.assertEquals(6L, professorIDs.stream().filter(ids -> ids.size() == 1).count());
        Assertions.assertEquals(1L, professorIDs.stream().filter(ids -> ids.size() == 2).count());
    }

    @Test
    void testOneGroupWithOneProfessorAndOneAssistantEN() {
        SubjectAllocationStats sa = new SubjectAllocationStats();
        List<CourseSize> courseSizeList = new ArrayList<>();
        List<TeacherSubjectAllocations> teacherSubjectAllocationsList = new ArrayList<>();

        buildSubject(sa, courseSizeList, teacherSubjectAllocationsList, 1,
                List.of(1F, 0F),
                List.of(0F, 1F)
        );

        courseSizeList.forEach(group -> group.setEnglish(true));
        teacherSubjectAllocationsList.forEach(teacher -> teacher.setEnglishGroup(true));

        CoursePreference coursePreference = new CoursePreference();
        coursePreference.setLectureSharing(LectureSharing.TOPICS);
        coursePreference.setAuditoriumExercisesSharing(LectureSharing.TOPICS);

        when(coursePreferenceRepository.findCoursePreferencesBySubject_Abbreviation(Mockito.any()))
                .thenReturn(List.of(coursePreference));
        when(roomRepository.findByCapacityGreaterThanOrderByCapacityAsc(Mockito.any()))
                .thenReturn(new ArrayList<>());

        List<Course> groups = courseService.calculateGroupsForSubject(sa, courseSizeList, teacherSubjectAllocationsList);

        Assertions.assertEquals(1, groups.size());
        Assertions.assertEquals(teacherSubjectAllocationsList.get(0).getProfessor().getId(), groups.get(0).getProfessors());
        Assertions.assertEquals(teacherSubjectAllocationsList.get(1).getProfessor().getId(), groups.get(0).getAssistants());
    }

    @Test
    void testFourGroupsHavingSharedLecturesAndSharedExercisesWithTwoProfessorsAndOneProfessorAssistantAndTwoAssistantsEN() {
        SubjectAllocationStats sa = new SubjectAllocationStats();
        List<CourseSize> courseSizeList = new ArrayList<>();
        List<TeacherSubjectAllocations> teacherSubjectAllocationsList = new ArrayList<>();

        buildSubject(sa, courseSizeList, teacherSubjectAllocationsList, 4,
                List.of(1F, 1F,   2F,   0F),
                List.of(0F, 1F, 1.5F, 1.5F)
        );

        courseSizeList.get(1).setEnglish(true);
        teacherSubjectAllocationsList.get(2).setEnglishGroup(true);
        teacherSubjectAllocationsList.get(3).setEnglishGroup(true);

        CoursePreference coursePreference = new CoursePreference();
        coursePreference.setLectureSharing(LectureSharing.TOPICS);
        coursePreference.setAuditoriumExercisesSharing(LectureSharing.TOPICS);

        when(coursePreferenceRepository.findCoursePreferencesBySubject_Abbreviation(Mockito.any()))
                .thenReturn(List.of(coursePreference));
        when(roomRepository.findByCapacityGreaterThanOrderByCapacityAsc(Mockito.any()))
                .thenReturn(new ArrayList<>());

        List<Course> groups = courseService.calculateGroupsForSubject(sa, courseSizeList, teacherSubjectAllocationsList);

        Assertions.assertEquals(4, groups.size());
        List<Course> groupsEN = groups.stream().filter(Course::getEnglish).toList();
        List<Course> groupsMKD = groups.stream().filter(group -> !group.getEnglish()).toList();
        groupsEN.forEach(
                group -> {
                    Assertions.assertEquals(teacherSubjectAllocationsList.get(2).getProfessor().getId(), group.getProfessors());
                    Assertions.assertEquals(2, group.getAssistants().split(";").length);
                }
        );
        groupsMKD.forEach(
                group -> {
                    Assertions.assertEquals(3, group.getProfessors().split(";").length);
                    Assertions.assertEquals(3, group.getAssistants().split(";").length);
                }
        );
    }

    private static List<CourseSize> joinedGroups(Integer... firstTimeEnrollments) {
        return Stream.of(firstTimeEnrollments)
                .map(it -> {
                    CourseSize js = new CourseSize();
                    js.setFirstTimeEnrollments(it.longValue());
                    return js;
                })
                .toList();
    }

    @Test
    void testCombineGroups() {
        List<CourseSize> groups = joinedGroups(2, 2, 1, 3, 2, 15, 5, 31, 2, 5, 21, 21, 4, 42, 94, 99, 66, 78, 79, 67, 29, 92, 4, 60, 95);

        List<List<CourseSize>> res = courseService.combineGroups(12, groups, false);
        Assertions.assertEquals(12, res.size());

        for (int i = 11; i >= 0; --i) {
            res = courseService.combineGroups(i, groups, false);
            Assertions.assertEquals(i, res.size());
        }

        for (int i = 5; i <= 20; ++i) {

            for (int j = 0; j <= i; ++j) {
                groups.get(j).setEnglish(true);
            }

            res = courseService.combineGroups(5, groups, true);
            Assertions.assertEquals(5, res.size());
            res.forEach(group -> {
                boolean allEN = group.stream().allMatch(it -> it.getEnglish() != null && it.getEnglish());
                boolean allMKD = group.stream().allMatch(it -> it.getEnglish() == null || !it.getEnglish());
                Assertions.assertTrue(allEN || allMKD);
            });
        }
    }
}
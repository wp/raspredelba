# Raspredelba

## Description

Raspredelba is a comprehensive scheduling system designed to streamline the complex process of allocating teachers to
subjects, calculating student groups, assigning classrooms and etc. for specific semesters at FINKI. This project aims
to optimize and automate the scheduling workflow, ensuring efficient use of resources and providing a user-friendly
interface for managing academic timetables.

## Reference implementation

- [CRUD operations with filters and paging](https://gitlab.finki.ukim.mk/wp/raspredelba/-/tree/main/src/main/java/mk/ukim/finki/backend/web/JoinedSubjectController.java)
- [Importing/Exporting data](https://gitlab.finki.ukim.mk/wp/raspredelba/-/tree/main/src/main/java/mk/ukim/finki/backend/web/CourseController.java)
- [Ajax calls](https://gitlab.finki.ukim.mk/wp/raspredelba/-/tree/main/src/main/resources/templates/courses/list.html): `saveCourse` javascript function
- [Redirect to my page](https://gitlab.finki.ukim.mk/wp/raspredelba/-/tree/main/src/main/java/mk/ukim/finki/backend/web/ProfessorEngagementController.java): `myEngagements` method

## Configuring Layout

This section explains how to configure the layout system using Thymeleaf as the frontend template engine. The layout
system enhances the structure and presentation of the application by providing a consistent layout across multiple
pages.

### Integration Steps

Incorporate the layout system files from the external project into your project. You can find the relevant files in the
following directory:
[Layout System Files](https://gitlab.finki.ukim.mk/wp/raspredelba/-/tree/dev/src/main/resources/templates)

- layout.html
- fragments/*.html
- joined-subjects/listJoinedSubjects.html (example of using the layout and paging)

In your `Application.java` file, add the following bean definition to enable the LayoutDialect:

```java
@Bean
public LayoutDialect layoutDialect(){
        return new LayoutDialect();
        }
```

This ensures that your application recognizes and applies the layout system.

Modify your pom.xml file to include the Thymeleaf Layout Dialect dependency:

```xml

<dependency>
    <groupId>nz.net.ultraq.thymeleaf</groupId>
    <artifactId>thymeleaf-layout-dialect</artifactId>
</dependency>
```

This dependency is crucial for Thymeleaf to understand and process the layout-related directives.

Ensure that every page includes the necessary Thymeleaf declarations at the beginning of the HTML file:

```html

<html xmlns:th="http://www.thymeleaf.org"
      xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"
      layout:decorate="~{layout}">
```

This declares the Thymeleaf and layout namespaces and specifies the layout to decorate.

Within your pages, use the following code to define the content fragment:

```html

<div class="mt-4" layout:fragment="content">
    <!-- Your page content goes here -->
</div>
```

This structure allows the layout system to inject your specific page content into the designated area.

### Example Usage

To see an example of the layout system and paging in action, you can refer to
the [joined-subjects/listJoinedSubjects.html](https://gitlab.finki.ukim.mk/wp/raspredelba/-/blob/dev/src/main/resources/templates/joined-subjects/listJoinedSubjects.html)
file.

## Paging

This section describes how to implement paging. Pagination is crucial for presenting large datasets in manageable
chunks, enhancing the user experience and optimizing performance.

### Integration steps

Ensure your repository interfaces extend the `JpaSpecificationRepository<T, ID>` interface, which includes methods
supporting specifications and pagination.

```java

@NoRepositoryBean
public interface JpaSpecificationRepository<T, ID> extends JpaRepository<T, ID> {
    Page<T> findAll(Specification<T> filter, Pageable page);

    List<T> findAll(Specification<T> filter);
}
```

In your service class, implement a method to retrieve paginated results based on specified criteria. Here is an example:

```java
@Override
public Page<ProfessorEngagement> findPage(String professor,String semester,String subject,
        Integer pageNum,Integer results){
        Specification<ProfessorEngagement> spec=Specification
        .where(filterEquals(ProfessorEngagement.class,"professor.id",professor))
        .and(filterEquals(ProfessorEngagement.class,"semester.code",semester))
        .and(filterEquals(ProfessorEngagement.class,"subject.abbreviation",subject));

        return repository.findAll(spec,PageRequest.of(pageNum-1,results,
        Sort.by(Sort.Direction.DESC,"semester.startDate")
        .and(Sort.by("professor.id","subject.abbreviation","classType"))));
        }
```

This method constructs a dynamic query using specifications and retrieves a specific page of data based on the provided
parameters.

In your controller, utilize the paginated results provided by the service layer and pass them to the view.

```java
public String engagementsByProfessor(@PathVariable(required = false) String professor,
@RequestParam(required = false) String semester,
@RequestParam(required = false) String subject,
@RequestParam(defaultValue = "1") Integer pageNum,
@RequestParam(defaultValue = "20") Integer results,
        Model model){
        Page<ProfessorEngagement> page=service.findPage(professor,semester,subject,pageNum,results);

        model.addAttribute("page",page);

        return"engagement/list";
        }
```

To incorporate the paging functionality into your HTML file, add the following line:

```html

<div th:include="fragments/paging :: pageSection(${page})"></div>
```

### Example usage

The implementation of pagination in
the [ProfessorEngagementController](https://gitlab.finki.ukim.mk/wp/raspredelba/-/blob/main/src/main/java/mk/ukim/finki/backend/web/ProfessorEngagementController.java)
demonstrates how users can navigate through paginated results. Explore this example to understand how pagination
enhances the organization and accessibility of data.

## Filters with specifications

This section explains how to implement filters with specifications in your Java Spring application using
the `JpaSpecificationRepository` interface. This approach enables you to dynamically define filtering criteria for your
repository queries, providing flexibility in data retrieval.

### Integration Steps

Ensure that your repository interfaces extend the `JpaSpecificationRepository<T, ID>` interface, which introduces
methods supporting specifications. This interface builds upon the standard `JpaRepository` from Spring Data JPA.

```java

@NoRepositoryBean
public interface JpaSpecificationRepository<T, ID> extends JpaRepository<T, ID> {
    Page<T> findAll(Specification<T> filter, Pageable page);

    List<T> findAll(Specification<T> filter);
}
```

Update your repository interface, for example the  `CourseRepository`, to extend
the `JpaSpecificationRepository`. This step incorporates the specification capabilities into your repository.

```java

@Repository
public interface CourseRepository extends JpaSpecificationRepository<Course, String> {
}

```

Utilize the specifications in your service layer to dynamically filter data based on user-provided criteria. Here's an
example for service implementation:

```java
@Override
public Page<Course> list(String semesterCode,String subject,String groupName,String professor,Integer pageNum,Integer results){
        Specification<Course> spec=Specification
        .where(filterEquals(Course.class,"semester.code",semesterCode))
        .and(filterEquals(Course.class,"joinedSubject.abbreviation",subject))
        .and(filterContainsText(Course.class,"groups",groupName))
        .and(filterContainsText(Course.class,"professors",professor))
        .or(filterContainsText(Course.class,"assistants",professor));
        return repository.findAll(spec,PageRequest.of(pageNum-1,results,
        Sort.by("semester.code","joinedSubject.name","professors")));
        }
```

Finally, leverage the dynamic filtering capabilities in your controller to retrieve and present paginated results based
on user input.

```java
    @GetMapping
    public String list(Model model,
    @RequestParam(required = false) String semester,
    @RequestParam(required = false) String joinedSubject,
    @RequestParam(required = false) String groupName,
    @RequestParam(required = false) String professor,
    @RequestParam(defaultValue = "1") Integer pageNum,
    @RequestParam(defaultValue = "20") Integer results) {

        Page<Course> result=service.list(semester,joinedSubject,groupName,professor,pageNum,results);

        model.addAttribute("page",result);

        return "courses/list";
        }
```

### Example Usage

For a practical demonstration of filters with specifications, examine
the [CourseRepository](https://gitlab.finki.ukim.mk/wp/raspredelba/-/blob/main/src/main/java/mk/ukim/finki/backend/repository/CourseRepository.java), [CourseService](https://gitlab.finki.ukim.mk/wp/raspredelba/-/blob/main/src/main/java/mk/ukim/finki/backend/service/implementation/JoinedSubjectServiceImpl.java),
and [CourseController](https://gitlab.finki.ukim.mk/wp/raspredelba/-/blob/main/src/main/java/mk/ukim/finki/backend/web/CourseController.java)
components in the codebase.

## Importing Data

This section guides you through implementing data import functionality in your Java Spring application using the
provided `ImportRepository` and `CsvImportRepository` examples.

### Integration steps

Start by defining the `ImportRepository` interface, which includes methods for reading and writing enrollments:

```java
public interface ImportRepository {
    <T> List<T> readEnrolments(MultipartFile file, Class<T> entityType);

    <T> void writeEnrollments(Class<T> entityType, List<T> enrollments, OutputStream outputStream);
}
```

This interface serves as a contract for importing and exporting data.

Implement the `CsvImportRepository` class, providing logic for reading and writing enrollments using the CSV format:

```java

@Repository
public class CsvImportRepository implements ImportRepository {

    private CsvMapper mapper = new CsvMapper();

    @Override
    public <T> List<T> readEnrolments(MultipartFile file, Class<T> clazz) {
        List<T> enrollments = new ArrayList<>();
        CsvSchema schema = mapper.schemaFor(clazz)
                .withHeader()
                .withLineSeparator("\n")
                .withColumnSeparator('\t');

        try (BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            MappingIterator<T> r = mapper
                    .reader(clazz)
                    .with(schema)
                    .readValues(br);
            while (r.hasNext()) {
                enrollments.add(r.nextValue());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return enrollments;.
    }

    @Override
    public <T> void writeEnrollments(Class<T> clazz, List<T> invalidEnrollments, OutputStream outputStream) throws IOException {
        // Implementation details for exporting data
    }
}
```

This repository utilizes the Jackson CSV module (`CsvMapper`) for handling CSV data. It includes methods to read data
from a file and write invalid enrollments to an output stream.

Create a controller endpoint that triggers the data import process:

```java
@PostMapping("/import")
public void importStudents(@RequestParam("file") MultipartFile file,HttpServletResponse response){
        // Implementation details
        }
```

The following line invokes the `readEnrolments` method from the `ImportRepository` interface, using
the `CsvImportRepository` implementation. It reads the contents of the provided CSV file (file) and maps them to a list
of `Course` entities.

```java
List<Course> students=importRepository.readEnrolments(file,Course.class);
```

The imported data is then processed by the service layer (`service.importData`). This method handles any business logic
related to the imported data and returns a list of invalid enrollments.

```java
List<Course> invalidEnrollments=service.importData(students);
```

Then we configure the HTTP response to prompt the user for downloading the processed data. It sets the response content
type to TSV (tab-separated values) and specifies the filename for the downloaded file. The final step involves writing
the list of invalid enrollments to the response output stream. This ensures that the processed data is available for
download as a TSV file. Any `IOException `during this process is caught and transformed into a `RuntimeException` for
simplicity.

```java
String fileName="invalid_students.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition","attachment; filename=\""+fileName+"\"");

        try(OutputStream outputStream=response.getOutputStream()){
        importRepository.writeEnrollments(Course.class,invalidEnrollments,outputStream);
        }catch(IOException e){
        throw new RuntimeException(e);
        }
```

### Example Usage

For a hands-on exploration of the data import feature, you can
see [student-groups/listStudentGroups.html](https://gitlab.finki.ukim.mk/wp/raspredelba/-/blob/main/src/main/resources/templates/student-groups/listStudentGroups.html)
import form and the
corresponding [controller method](https://gitlab.finki.ukim.mk/wp/raspredelba/-/blob/main/src/main/java/mk/ukim/finki/backend/web/CourseController.java)
and [repository](https://gitlab.finki.ukim.mk/wp/raspredelba/-/blob/main/src/main/java/mk/ukim/finki/backend/repository/tsv/CsvImportRepository.java).

## Exporting Data

This section guides you through the process of implementing data export in your Java Spring application.

### Integration Steps

We need the `ImportRepository` and `CsvImportRepository` from the Importing Data section. We need to implement
the `writeEnrollments` method in the `CsvImportRepository` class:

```java
@Override
public<T> void writeEnrollments(Class<T> clazz,List<T> invalidEnrollments,OutputStream outputStream)throws IOException{
        CsvSchema schema=mapper.schemaFor(clazz)
        .withHeader()
        .withLineSeparator("\n")
        .withColumnSeparator('\t');
        mapper.writer(schema).writeValue(outputStream,invalidEnrollments);
        outputStream.flush();
        }
```

Ensure you have an endpoint configured to trigger the data export. In your controller:

```java
@GetMapping(value = {"/all", "/{professorId}"}, params = "export")
public void export(@PathVariable(required = false) String professorId,
@RequestParam(required = false) String professor,
@RequestParam(required = false) String semester,
@RequestParam(required = false) String subject,
@RequestParam(defaultValue = "1") Integer pageNum,
@RequestParam(defaultValue = "1000") Integer results,
        Model model,
        HttpServletResponse response){
        // Implementation details...
        }
```

This annotation defines a GET mapping for two possible endpoints (`/all` and `/{professorId}`), specifying that the
export parameter must be present to trigger the export functionality.

Invoke the necessary method to retrieve and prepare the data for export:

```java
String prof=Optional.ofNullable(professorId).orElse(professor);
        engagementsByProfessor(prof,semester,subject,pageNum,results,model);
        Page<ProfessorEngagement> page=(Page<ProfessorEngagement>)model.getAttribute("page");
        List<ProfessorEngagement> data=page.getContent();
```

Ensure that the `engagementsByProfessor` method retrieves and stores the paginated data in the model.

Generate a suitable file name for the exported file based on the provided parameters:

```java
String fileName=String.format("engagement-%s-%s.tsv",Optional.ofNullable(prof).orElse("all"),
        Optional.ofNullable(semester).orElse("all"));
```

Set up the response to trigger the browser's download functionality:

```java
response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition","attachment; filename=\""` +fileName+"\"");
```

Write the exported data to the response output stream:

```java
try(OutputStream outputStream=response.getOutputStream()){
        importRepository.writeEnrollments(ProfessorEngagement.class,data,outputStream);
        }catch(IOException e){
        throw new RuntimeException("Error exporting data",e);
        }
```

### Example Usage

Navigate to the specified endpoint in
the [ProfessorEngagementController](https://gitlab.finki.ukim.mk/wp/raspredelba/-/blob/main/src/main/java/mk/ukim/finki/backend/web/ProfessorEngagementController.java)
with the `export` parameter to observe the export functionality in action. On
the [/engagement/all](https://gitlab.finki.ukim.mk/wp/raspredelba/-/blob/dev/src/main/resources/templates/engagement/list.html)
endpoint, when the export checkbox is selected and the filter button is clicked, the exported file will be downloaded
with a filename based on the specified parameters, containing the relevant data from the paginated and filtered results.
